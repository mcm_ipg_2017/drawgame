package com.toymobi.drawgame_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.drawgame.GameDrawingFrag;
import com.toymobi.recursos.BaseFragment;

public class DrawGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.drawgame_activity);

        startFragment();
    }

    private void startFragment() {

        GameDrawingFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment fragmentGestualBook = new GameDrawingFrag();

        fragmentTransaction.add(R.id.fragment_container, fragmentGestualBook, GameDrawingFrag.FRAGMENT_TAG_GAME_DRAWING);

        fragmentTransaction.commit();
    }
}
