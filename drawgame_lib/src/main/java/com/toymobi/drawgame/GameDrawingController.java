package com.toymobi.drawgame;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.SparseIntArray;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.toymobi.framework.dialog.IconizedMenu;
import com.toymobi.framework.feedback.VibrationFeedback;
import com.toymobi.framework.media.SimpleSoundPool;
import com.toymobi.framework.options.GlobalSettings;

class GameDrawingController implements OnClickListener {

    private static final int[] BUTTONS_DRAWING_COLOR = {R.id.button_black,
            R.id.button_brown, R.id.button_green, R.id.button_indigo,
            R.id.button_light_blue, R.id.button_orange, R.id.button_pink,
            R.id.button_purple, R.id.button_red, R.id.button_yellow};

    private static final int[] BUTTONS_DRAWING_ITEM = {R.id.button_eraser,
            R.id.button_brush};

    private static final int[] BUTTONS_GEOMETRIC_ITEM_DRAW = {
            R.id.button_square, R.id.button_circle, R.id.button_triangle,
            R.id.button_dot};

    private final SparseIntArray colorsArray = new SparseIntArray(BUTTONS_DRAWING_COLOR.length);

    private static final int INVALID_COLOR = -9999;

    private static final int ITEM_ERASER = 0;
    private static final int ITEM_BRUSH = ITEM_ERASER + 1;

    private static final int ITEM_SIZE_SMALL = 0;
    private static final int ITEM_SIZE_MEDIUM = ITEM_SIZE_SMALL + 1;
    private static final int ITEM_SIZE_LARGE = ITEM_SIZE_MEDIUM + 1;

    private Context context;

    private VibrationFeedback vibrationFeedback;

    private SimpleSoundPool sfx;

    private Toolbar toolbar;

    private DrawGameView drawView;

    private float smallSize, mediumSize, largeSize;

    private float brushSize, eraserSize;

    GameDrawingController(final Context context) {
        if (context != null) {
            this.context = context;

            if (vibrationFeedback == null) {
                vibrationFeedback = new VibrationFeedback(context);
            }

            if (sfx == null) {
                sfx = new SimpleSoundPool(context, R.raw.sfx_normal_click);
            }

            smallSize = context.getResources().getInteger(
                    R.integer.small_size);

            mediumSize = context.getResources().getInteger(
                    R.integer.medium_size);

            largeSize = context.getResources().getInteger(
                    R.integer.large_size);

            brushSize = mediumSize;

            eraserSize = mediumSize;
        }
    }

    void createButtonsColor(final View view) {
        if (view != null && colorsArray.size() == 0) {
            for (final int id : BUTTONS_DRAWING_COLOR) {
                final View viewButton = view.findViewById(id);
                if (viewButton != null) {
                    final String color = viewButton.getTag().toString();
                    if (color.length() > 0) {
                        colorsArray.put(id, Color.parseColor(color));
                        viewButton.setOnClickListener(this);
                    }
                }
            }

            for (final int id : BUTTONS_DRAWING_ITEM) {
                final View viewButton = view.findViewById(id);
                if (viewButton != null) {
                    viewButton.setOnClickListener(this);
                }
            }

            for (final int id : BUTTONS_GEOMETRIC_ITEM_DRAW) {
                final View viewButton = view.findViewById(id);
                if (viewButton != null) {
                    viewButton.setOnClickListener(this);
                }
            }

        }
    }

    void createDrawView(final View view) {
        if (view != null && drawView == null) {
            drawView = view.findViewById(R.id.drawing);
            final float mediumBrush = context.getResources().getInteger(
                    R.integer.medium_size);
            drawView.setBrushSize(mediumBrush);
        }
    }

    void setInitialToolBarColor() {
        setColor(colorsArray.get(R.id.button_light_blue));
    }

    final void deallocate() {
        colorsArray.clear();

        if (sfx != null) {
            sfx.release();
        }


    }

    @Override
    public final void onClick(final View view) {
        if (view != null) {

            final int viewId = view.getId();

            if (viewId == R.id.button_eraser) {
                setSizeItem(ITEM_ERASER, eraserSize);
                onPopupButtonClick(view, ITEM_ERASER);
            } else if (viewId == R.id.button_brush) {
                setSizeItem(ITEM_BRUSH, brushSize);
                onPopupButtonClick(view, ITEM_BRUSH);
            } else if (viewId == R.id.button_square) {
                if (drawView != null) {
                    drawView.drawType = DrawGameView.DRAW_SQUARE;
                    setSizeItem(ITEM_BRUSH, brushSize);
                }
            } else if (viewId == R.id.button_circle) {
                if (drawView != null) {
                    drawView.drawType = DrawGameView.DRAW_CIRCLE;
                    setSizeItem(ITEM_BRUSH, brushSize);
                }
            } else if (viewId == R.id.button_triangle) {
                if (drawView != null) {
                    drawView.drawType = DrawGameView.DRAW_TRIANGLE;
                    setSizeItem(ITEM_BRUSH, brushSize);
                }
            } else if (viewId == R.id.button_dot) {
                if (drawView != null) {
                    drawView.drawType = DrawGameView.DRAW_DOT;
                    setSizeItem(ITEM_BRUSH, brushSize);
                }
            } else {
                final int colorSelected = colorsArray.get(viewId, INVALID_COLOR);

                if (colorSelected != INVALID_COLOR) {
                    setColor(colorSelected);
                } else {
                    setColor(colorSelected);
                }
            }
        }
    }

    final void playFeedbackButton() {
        if (vibrationFeedback != null) {
            vibrationFeedback.vibrate();
        }

        if (sfx != null && GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_normal_click);
        }
    }

    private void setColor(final int color) {
        if (toolbar != null) {

            final ColorDrawable colorDrawable = new ColorDrawable(color);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                setColorOldAPI(colorDrawable);
            } else {
                setColorNewApi(colorDrawable);
            }
        }

        if (drawView != null) {
            drawView.setErase(false);
            drawView.setBrushSize(brushSize);
            drawView.setColor(color);
        }
    }

    void newPaper() {
        if (drawView != null) {
            drawView.startNewPaper();
        }
    }

    private void onPopupButtonClick(final View button, final int itemType) {
        if (context != null && button != null) {

            final IconizedMenu popup = new IconizedMenu(context, button);

            popup.getMenuInflater().inflate(R.menu.menu_game_drawing_choose_brush, popup.getMenu());

            popup.setOnMenuItemClickListener(new IconizedMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(final MenuItem item) {

                    if (item != null) {
                        final int item_id = item.getItemId();

                        if (item_id == R.id.brush_small_icon) {
                            setSizeItem(itemType, ITEM_SIZE_SMALL);
                        } else if (item_id == R.id.brush_medium_icon) {
                            setSizeItem(itemType, ITEM_SIZE_MEDIUM);
                        } else if (item_id == R.id.brush_large_icon) {
                            setSizeItem(itemType, ITEM_SIZE_LARGE);
                        }
                    }
                    return true;
                }
            });
            popup.show();
        }
    }

    private void setSizeItem(final int item, final int size) {
        float sizeItem;

        switch (size) {
            case ITEM_SIZE_SMALL:
                sizeItem = smallSize;
                break;

            case ITEM_SIZE_MEDIUM:
                sizeItem = mediumSize;
                break;

            case ITEM_SIZE_LARGE:
                sizeItem = largeSize;
                break;

            default:
                sizeItem = mediumSize;
                break;
        }

        if (item == ITEM_ERASER) {
            drawView.drawType = DrawGameView.DRAW_DOT;
            eraserSize = sizeItem;
            drawView.setErase(true);
            drawView.setBrushSize(eraserSize);
        } else if (item == ITEM_BRUSH) {
            brushSize = sizeItem;
            if (drawView != null) {
                drawView.setErase(false);
                drawView.setBrushSize(brushSize);
//                drawView.setLastBrushSize(brushSize);
            }
        }
    }

    private void setSizeItem(final int item, final float size) {
        if (item == ITEM_ERASER) {
            drawView.drawType = DrawGameView.DRAW_DOT;
            eraserSize = size;
            drawView.setErase(true);
            drawView.setBrushSize(eraserSize);
        } else if (item == ITEM_BRUSH) {
            brushSize = size;
            if (drawView != null) {
                drawView.setErase(false);
                drawView.setBrushSize(brushSize);
//                drawView.setLastBrushSize(brushSize);
            }
        }
    }

    void setToolbar(@NonNull Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void setColorOldAPI(final ColorDrawable colorDrawable) {
        if (toolbar != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                toolbar.setBackgroundDrawable(colorDrawable);
            }
        }
    }

    private void setColorNewApi(final ColorDrawable colorDrawable) {
        if (toolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                toolbar.setBackground(colorDrawable);
            }
        }
    }
}
