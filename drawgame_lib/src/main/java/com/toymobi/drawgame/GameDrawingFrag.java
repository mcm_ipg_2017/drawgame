package com.toymobi.drawgame;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;

import com.toymobi.recursos.BaseFragment;

public class GameDrawingFrag extends BaseFragment {


    public static final String FRAGMENT_TAG_GAME_DRAWING = "FRAGMENT_TAG_GAME_DRAWING";

    private GameDrawingController gameDrawingController;

    private Toolbar toolbar;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (gameDrawingController == null) {
            gameDrawingController = new GameDrawingController(getActivity());
        }

        createMusicBackground(R.string.path_sound_game_menu, R.raw.minigame_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.drawgame_fragment, container, false);

        createToolBar(R.id.toolbar_draw, R.string.drawgame_toolbar_name);

        toolbar = getToolBar(R.id.toolbar_draw);

        if (gameDrawingController != null) {
            gameDrawingController.createButtonsColor(mainView);
            gameDrawingController.createDrawView(mainView);
            gameDrawingController.setToolbar(toolbar);
            gameDrawingController.setInitialToolBarColor();
        }

        return mainView;
    }

    @Override
    public final void onDestroy() {
        super.onDestroy();

        backActionBarColor();

        deallocate();
    }

    @SuppressWarnings("deprecation")
    private void backActionBarColor() {
        if (toolbar != null) {
            final int colorActionBar = ResourcesCompat.getColor(getResources(), R.color.ACTION_BAR_COLOR, null);
            final ColorDrawable colorDrawable = new ColorDrawable(colorActionBar);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                setColorOldAPI(colorDrawable);
            } else {
                setColorNewApi(colorDrawable);
            }
        }
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_game_drawing, menu);

        itemSound = menu.findItem(R.id.sound_drawing_menu);

        setIconSoundMenu();
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int item_id = item.getItemId();

        playFeedBackButtons();

        if (gameDrawingController != null) {
            gameDrawingController.playFeedbackButton();
        }

        if (item_id == R.id.exit_drawing_menu) {

            exitOrBackMenu(startSingleApp);

        } else if (item_id == R.id.drawing_new_menu) {

            if (gameDrawingController != null) {
                gameDrawingController.newPaper();
            }
        } else if (item_id == R.id.sound_drawing_menu) {

            changeSoundMenu();

        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void deallocate() {

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }

        if (gameDrawingController != null) {
            gameDrawingController.deallocate();
            gameDrawingController = null;
        }

        super.deallocate();
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void setColorOldAPI(final ColorDrawable colorDrawable) {
        if (toolbar != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                toolbar.setBackgroundDrawable(colorDrawable);
            }
        }
    }

    private void setColorNewApi(final ColorDrawable colorDrawable) {
        if (toolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                toolbar.setBackground(colorDrawable);
            }
        }
    }
}
